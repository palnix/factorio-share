## Nauvis - Full Base with Trainstop Labels

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/Map_w_train_labels.png)

## Nauvis - Full Base No Labels

Try to ignore all the crazy tracks, I expiremented a lot with different methods.  The slanted outbound stacker with rounded inbound stacker is my current build.

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/Map.png)

## Copper Smelting

This smelter design is what I mainly use, and is the last smelting tech needed.  I used the normal smelting array up till this tech unlock.

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/copper_smelting.png)

## Green Chips

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/green_chips.png)

## Factory Origin

This spaghetti is the original factory.

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/original_base.png)

## First colonzied planet - Vulcanite

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/vulcan.png)

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/vulcan_launch.png)

## Second Colonized planet - Cryonite and Beryllium

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/Cryo_and_beryl.png)

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/beryl.png)

## Fuel Moon

I use this moon as a way to fuel the whole system. It processes all the oil and ships it as rocket fuel.

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/Fuel.png)

![alt text](https://gitlab.com/palnix/factorio-share/-/raw/main/images/fuel_launch.png)
